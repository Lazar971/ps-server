/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package config;

import constant.Konstante;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.util.Properties;

/**
 *
 * @author Lazar
 */
public class Konfiguracija {
    
    Properties properties=new Properties();
    private static Konfiguracija instance;
    private Konfiguracija() throws  IOException {
        FileInputStream fs=new FileInputStream(Konstante.DB_CONFIG);
        properties.load(fs);
    }
    
    public static Konfiguracija getInstance() throws IOException {
        if(instance==null){
            instance=new Konfiguracija();
        }
        return instance;
    }

    public String getURL(){
        return properties.getProperty(Konstante.URL);
        
    }
    public int getPort(){
        return Integer.parseInt(properties.getProperty(Konstante.PORT));
    }

    public String getDBUser() {
        return properties.getProperty(Konstante.USERNAME);
    }

    public String getDBPass() {
        return properties.getProperty(Konstante.PASSWORD);
    }

    public void setURL(String url) throws IOException {
        properties.setProperty(Konstante.URL, url);
        save();
    }

    public void setPort(int parseInt) throws IOException {
        properties.setProperty(Konstante.PORT, parseInt+"");
        save();
    }

    public void setPass(String pass) throws IOException {
        properties.setProperty(Konstante.PASSWORD, pass);
        save();
    }

    public void setUser(String user) throws IOException {
        properties.setProperty(Konstante.USERNAME, user);
        save();
    }
    
    private void save() throws IOException{
        FileOutputStream fos = new FileOutputStream(Konstante.DB_CONFIG);
        properties.store(fos, "");
        fos.close();
    }
    
    
    
}
