/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package constant;

/**
 *
 * @author Lazar
 */
public class Konstante {
    
    public static final String DB_CONFIG="config.properties";
    public static final String USERNAME="username";
    public static final String PASSWORD="password";
    public static final String URL="url";
    public static final String PORT="port";
    public static final String HOST="host";
}
