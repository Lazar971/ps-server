/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import config.Konfiguracija;
import db.connection.Konekcija;
import form.FormServer;
import java.io.IOException;
import java.net.ServerSocket;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import thread.ServerThread;

/**
 *
 * @author Lazar
 */
public class Controller {
    
    private static Controller instance;
    private ServerThread serverThread;
    
    private Controller(){
        
    }
    public static Controller getInstance(){
        if(instance==null){
            instance=new Controller();
        }
        return instance;
    }
    public void startServer(){
        try {
            this.serverThread=new ServerThread();
            serverThread.start();
            System.out.println("Server started at port: "+serverThread.getServerSocket().getLocalPort());
        } catch (IOException ex) {
           ex.printStackTrace();
        }
        
    }
    public void stopServer(){
        if(serverThread !=null && serverThread.getServerSocket()!=null && !serverThread.getServerSocket().isClosed()){
            try {
                serverThread.getServerSocket().close();
                System.out.println("Server stoped");
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        serverThread=null;
    }

    public String getDatabaseURL() {
        try {
            return Konfiguracija.getInstance().getURL();
        } catch (IOException ex) {
            return "greska";
        }

        
    }

    public String getDatabaseUser() {
        try {
            return Konfiguracija.getInstance().getDBUser();
        } catch (IOException ex) {
            return "";
        }
    }

    public String getPort() {
        try {
            return Konfiguracija.getInstance().getPort()+"";
        } catch (IOException ex) {
            return "";
        }
    }

    public String getDBPassword() {
        try {
            return Konfiguracija.getInstance().getDBPass();
        } catch (IOException ex) {
            return "Error";
        }
    }

    public void testConnection(JFrame frame,String url,String user,String pass) {
        try {
            System.out.println("URL: "+url);
            System.out.println("User: "+user);
            System.out.println("Pass: "+pass);
            Connection conn=DriverManager.getConnection(url, user, pass);
            JOptionPane.showMessageDialog(frame, "Uspeh");
            conn.close();
        }
          catch (SQLException ex) {
            JOptionPane.showMessageDialog(frame, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void testPort(FormServer frame, String text) {
        try {
            int port=Integer.parseInt(text);
            ServerSocket serverSocket=new ServerSocket(port);
            JOptionPane.showMessageDialog(frame, "Uspeh");
            serverSocket.close();
        } catch (Exception ex) {
           JOptionPane.showMessageDialog(frame, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void updateConfigParams(FormServer frame, String url, String user, String pass, String port) {
        try {
            Konfiguracija.getInstance().setURL(url);
            Konfiguracija.getInstance().setUser(user);
            Konfiguracija.getInstance().setPass(pass);
            Konfiguracija.getInstance().setPort(Integer.parseInt(port));
            JOptionPane.showMessageDialog(frame, "Uspeh");
        } catch (Exception ex) {
           JOptionPane.showMessageDialog(frame, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }
    }
}
