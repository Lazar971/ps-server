/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db.connection;

import config.Konfiguracija;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Lazar
 */
public class Konekcija {
    
    private Connection connection;
    private static Konekcija INSTANCE;
    private Konekcija() throws IOException, SQLException {
        Konfiguracija konfiguracija=Konfiguracija.getInstance();
        String url=konfiguracija.getURL();
        String user=konfiguracija.getDBUser();
        String pass=konfiguracija.getDBPass();
        connection=DriverManager.getConnection(url,user,pass);
        
    }
    
    public static Konekcija getInstance() throws IOException, SQLException {
        if(INSTANCE==null){
            INSTANCE=new Konekcija();
        }
        return INSTANCE;
    }
    
    public Connection getConnection() {
        return connection;
    }
    
    
}
