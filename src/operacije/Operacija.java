/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package operacije;

import db.DatabaseBroker;
import domain.DomainObject;
import transfer.ResponseObject;

/**
 *
 * @author Lazar
 */
public abstract class Operacija {
    
    protected DatabaseBroker broker;
    
    
    protected abstract void validate(DomainObject obj)throws Exception;
    
    public ResponseObject execute(DomainObject obj)throws Exception{
        
        try{
            validate(obj);
            return template(obj);
            }catch(Exception e){
                throw e;
        }
    }
    protected abstract ResponseObject template(DomainObject obj)throws Exception;
}
