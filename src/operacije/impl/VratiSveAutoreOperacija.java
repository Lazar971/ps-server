/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package operacije.impl;

import context.Context;
import db.DatabaseBroker;
import domain.DomainObject;
import domain.Pisac;
import java.util.List;
import operacije.Operacija;
import transfer.ResponseObject;

/**
 *
 * @author Lazar
 */
public class VratiSveAutoreOperacija extends Operacija{

    @Override
    protected void validate(DomainObject obj) throws Exception {

    }

    @Override
    protected ResponseObject template(DomainObject obj) throws Exception {
        List<DomainObject> pisci=Context.getInstance().getAutori();
        if(pisci==null){
            DatabaseBroker.getInstance().findAll(new Pisac(), pisci);
        }
        
        return new ResponseObject(0, pisci);
    }
    
}
