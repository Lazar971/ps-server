/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package thread;

import domain.DomainObject;
import transfer.RequestObject;
import transfer.ResponseObject;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import operacije.Operacija;
import operacije.factory.OperacijaFactory;

/**
 *
 * @author Lazar
 */
public class ClientThread extends Thread{
    
    private Socket socket;
    private ObjectInputStream input;
    private ObjectOutputStream output;
    public ClientThread(Socket s) throws IOException {
        socket=s;
        input=new ObjectInputStream(socket.getInputStream());
        output=new ObjectOutputStream(socket.getOutputStream());
    }

    @Override
    public void run() {
        while(!socket.isClosed()){
            try {
                RequestObject req=(RequestObject)input.readObject();
                ResponseObject res=handleRequest(req);
                output.writeObject(res);
            } catch (IOException ex) {
                Logger.getLogger(ClientThread.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(ClientThread.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private ResponseObject handleRequest(RequestObject req) {
        Operacija operacija=OperacijaFactory.create(req.getOperacija());
        try {
            return operacija.execute((DomainObject) req.getData());
            
        } catch (Exception ex) {
            Logger.getLogger(ClientThread.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    
    
}
