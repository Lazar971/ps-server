/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package thread;

import config.Konfiguracija;
import constant.Konstante;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.ServerSocket;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Lazar
 */
public class ServerThread extends Thread{
    
    
    private  ServerSocket serverSocket;
    private int port;
    public ServerThread() throws IOException {
        try {
            port=Konfiguracija.getInstance().getPort();
       
        } catch (Exception ex) {
            ex.printStackTrace();
            port=9000;
        }
        this.serverSocket=new ServerSocket(port);
        
    }

    public ServerSocket getServerSocket() {
        return serverSocket;
    }

    public void setServerSocket(ServerSocket serverSocket) {
        this.serverSocket = serverSocket;
    }
    
    @Override
    public void run() {
        while(!serverSocket.isClosed()){
            try {
                ClientThread cl=new ClientThread(serverSocket.accept());
                
            } catch (IOException ex) {
                
            }
        }
    }
    
    
    
}
